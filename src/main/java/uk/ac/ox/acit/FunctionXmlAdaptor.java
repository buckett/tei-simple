package uk.ac.ox.acit;

import uk.ac.ox.acit.functions.Function;
import uk.ac.ox.acit.functions.FunctionParser;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by buckett on 19/03/15.
 */
public class FunctionXmlAdaptor extends XmlAdapter<String, Function> {
    @Override
    public Function unmarshal(String v) throws Exception {
        return FunctionParser.getInstance().parse(v);
    }

    @Override
    public String marshal(Function v) throws Exception {
        return null;
    }
}
