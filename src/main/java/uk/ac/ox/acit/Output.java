package uk.ac.ox.acit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * The valid set of outputs when processing.
 */
@XmlType
@XmlEnum(String.class)
public enum Output {
    @XmlEnumValue("web") WEB,
    @XmlEnumValue("print") PRINT,
    @XmlEnumValue("plaintext") PLAINTEXT;
}
