package uk.ac.ox.acit;

import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Created by buckett on 19/03/15.
 */
public class XPathXmlAdapter extends XmlAdapter<String,XPathExpression> {

    @Override
    public XPathExpression unmarshal(String v) throws Exception {
        XPathBuilder xPathBuilder = new XPathBuilder(v, Filters.fpassthrough());
        XPathExpression xPathExpression = xPathBuilder.compileWith(XPathFactory.instance());
        return xPathExpression;
    }

    @Override
    public String marshal(XPathExpression v) throws Exception {
        return v.getExpression();
    }
}
