package uk.ac.ox.acit;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.jdom2.JDOMException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * This is the main entry point for the command line processor.
 */
public class Processor {

    public static void main (String... argv) throws IOException {
        OptionParser parser = new OptionParser();
        OptionSpec help = parser.accepts("help", "Show command line help and exit.");
        OptionSpec<File> model = parser.accepts("model", "The model to process.").withRequiredArg().ofType(File.class).required();
        // Would be nice to allow this option multiple times.
        OptionSpec<File> file = parser.accepts("file", "The file to transform.").withRequiredArg().ofType(File.class);

        OptionSet set = parser.parse(argv);

        if (set.has(help)) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        File modelFile = set.valueOf(model);
        if ( !(modelFile.exists() && modelFile.isFile()) ) {
            System.out.println("Couldn't fine or open: "+ modelFile.getAbsolutePath());
        }
        try {
            SpecParser specParser = new SpecParser(modelFile);
            // TODO Should do validation at this point and strip the spec of everything that isn't related to the output we building for.
            Transformer transformer = new Transformer(specParser.getElements());
            transformer.transform(new FileInputStream(set.valueOf(file)), System.out);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
