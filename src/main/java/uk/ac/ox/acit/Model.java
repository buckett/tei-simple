package uk.ac.ox.acit;

import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;
import uk.ac.ox.acit.functions.Function;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Queue;

/**
 * Parsed model.
 */
public class Model {


    @XmlAttribute
    protected Boolean useSourceRendition;
    @XmlAttribute
    protected Output output;

    @XmlJavaTypeAdapter(FunctionXmlAdaptor.class)
    @XmlAttribute
    private Function behaviour;

    @XmlJavaTypeAdapter(XPathXmlAdapter.class)
    @XmlAttribute
    private XPathExpression predicate;


    private List<String> rendition;

    @Override
    public String toString() {
        return "Model{" +
                "useSourceRendition=" + useSourceRendition +
                ", output=" + output +
                ", behaviour=" + behaviour +
                ", predicate=" + ((predicate != null)?predicate.getExpression():null) +
                ", rendition=" + rendition +
                '}';
    }

    public boolean matches(Element element) {
        if (predicate == null) {
            return true;
        } else {
            // TODO Better debugging of this.
            return predicate.evaluate(element) != null;
        }
    }

    public void transform(Element element, OutputStream out) throws IOException {
        if (behaviour != null) {
            behaviour.enter(element, out);
        }
    }
}
