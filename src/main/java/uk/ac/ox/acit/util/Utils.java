package uk.ac.ox.acit.util;

import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
 * Created by buckett on 18/03/15.
 */
public class Utils {


    public static XPathExpression xpath(String string) {
        // TODO Caching.
        XPathBuilder xPathBuilder = new XPathBuilder(string, Filters.fpassthrough());
        XPathExpression xPathExpression = xPathBuilder.compileWith(XPathFactory.instance());
        return xPathExpression;
    }
}
