package uk.ac.ox.acit;

import org.jdom2.JDOMException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This parses the spec. Although this doesn't do low-level parsing this parser checks that
 * things make sense and pre-validates things so that when we want to process the source document
 * we don't have to walk the DOM to find out how we should be processing an element.
 */
public class SpecParser {


    private Map<String, ElementSpec> elements;

    public SpecParser(File source) throws IOException, XMLStreamException, JAXBException {
        elements = new HashMap<>();
        XMLInputFactory xif = XMLInputFactory.newFactory();
        StreamSource xml = new StreamSource(source);
        XMLStreamReader xsr = xif.createXMLStreamReader(xml);

        do {
            xsr.next();
            if (xsr.isStartElement() && xsr.getLocalName().equals("elementSpec")) {
                JAXBContext jc = JAXBContext.newInstance(ElementSpec.class);
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                JAXBElement<ElementSpec> jb = unmarshaller.unmarshal(xsr, ElementSpec.class);
                ElementSpec es = jb.getValue();
                elements.put(es.getIdent(), es);
                System.out.println(es);
            }
        } while (xsr.hasNext());

        xsr.close();

    }

    public Map<String, ElementSpec> getElements() {
        return elements;
    }
}
