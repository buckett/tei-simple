package uk.ac.ox.acit;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * A Group of models, this allows simpler ordering and specifying of default options.
 */
public class ModelGroup {

    @XmlAttribute
    protected Boolean useSourceRendition;
    @XmlAttribute
    protected Output output;

}
