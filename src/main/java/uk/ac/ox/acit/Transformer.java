package uk.ac.ox.acit;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * We can't use a streaming parser here as we need to easily be able to evaluate xpath expressions.
 */
public class Transformer {

    private final Map<String, ElementSpec> specs;

    public Transformer(Map<String, ElementSpec> specs) {
        this.specs = specs;
    }

    public void transform(InputStream in, OutputStream out) throws XMLStreamException, JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(in);
        // Treat the Queue as a stack.
        Queue<ElementSpec> stack = new LinkedList<>();
        // Iterate over the elements in the document.
        // Using this DOM based approach we don't get closing tags.
        for (Element element : document.getContent(Filters.element())) {
            String name = element.getName();
            ElementSpec spec = specs.get(name);
            if (spec != null) {
                spec.transform(element, System.out);
            }
        }
    }

    static class State {
        private final InputStream in;
        private final OutputStream out;

        State(InputStream in, OutputStream out) {
            this.in = in;
            this.out = out;
        }

        public InputStream getIn() {
            return in;
        }

        public OutputStream getOut() {
            return out;
        }
    }
}
