package uk.ac.ox.acit.functions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by buckett on 18/03/15.
 */
public class FunctionFactory {

    private Map<String, Function> functionMap;

    public FunctionFactory() {
        functionMap = new HashMap<>();

    }

    protected void registerStandardFunctions() {

    }

    protected void register(Function function) {
        functionMap.put(function.getName(), function);
    }
}
