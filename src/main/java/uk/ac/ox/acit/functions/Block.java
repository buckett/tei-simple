package uk.ac.ox.acit.functions;

import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by buckett on 18/03/15.
 */
public class Block extends Function {

    private XPathExpression xpath;

    public Block(XPathExpression xpath) {
        this.xpath = xpath;
    }

    @Override
    public String getName() {
        return "block";
    }

    @Override
    public void enter(Element node, OutputStream output) throws IOException {
        List results = xpath.evaluate(node);
        String text = "<div>";
        // This doesn't work as we need to iterate over all elements (text nodes) in the correct order.
        text += results.toString();
        for (Object result: results) {
            if (results instanceof Element) {
                Element element = (Element) result;
                text += element.getText();
            }
        }

        output.write(text.getBytes());
    }

    public void leave(Element node, OutputStream output) throws IOException {
        output.write("</div>".getBytes());
    }
}
