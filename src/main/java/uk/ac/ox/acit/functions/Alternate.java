package uk.ac.ox.acit.functions;

import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by buckett on 18/03/15.
 */
public class Alternate extends Function {


    private final XPathExpression alternate;
    private final XPathExpression preferred;

    public Alternate(XPathExpression preferred, XPathExpression alternate) {
        this.preferred = preferred;
        this.alternate = alternate;
    }

    @Override
    public String getName() {
        return "alternate";
    }

    @Override
    public void enter(Element node, OutputStream output) throws IOException {

    }

    @Override
    public void leave(Element node, OutputStream output) throws IOException {

    }

}
