package uk.ac.ox.acit.functions;

import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import javax.xml.xpath.XPath;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static uk.ac.ox.acit.util.Utils.xpath;

/**
 * This is a simple function parser, as the format is reasonably simple we just hack it.
 * The problem with this implementation is that we really need to implement a full xpath
 * parser as the specification allows for a function that accepts multiple xpath parameters
 * and an xpath parameter can contain a comma.
 *
 */
public class FunctionParser {

    private static FunctionParser instance;

    public FunctionParser() {
        pattern = Pattern.compile("\\s*(?<function>\\w+)\\s*(\\(\\s*(?<parameters>.*)\\)\\s*)?\\s*");
    }

    public static FunctionParser getInstance() {
        if (instance == null) {
            try {
                instance = new FunctionParser();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private Pattern pattern;

    public Function parse(String string) {
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            String function = matcher.group("function");
            String parameters = matcher.group("parameters");
            return lookupFunction(function, parameters);
        } else {
            // TODO Failed to understand function.
        }
        return null;
    }

    private Function lookupFunction(String function, String parameters) {
        switch (function) {
            case "alternate":
                // The worst one as it's multiple xpaths.
                String[] parts = parameters.split(",");
                if (parts.length == 2) {
                    return new Alternate(xpath(parts[0]), xpath(parts[1]));
                } else {
                    throw new RuntimeException("Wrong arguments"); // TODO Exception hierarchy.
                }
            case "anchor":
                return new Anchor(xpath(parameters));
            case "block":
                return new Block(xpath(parameters));
            default:
                throw new RuntimeException("Unknown function: "+ function);
        }
    }

}
