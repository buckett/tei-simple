package uk.ac.ox.acit.functions;

import org.jdom2.Element;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Functions are event based processors of the source document.
 */
abstract public class Function {

    /**
     * This returns the name of the function.
     * @return The name.
     */
    abstract public String getName();

    /**
     * Process start
     * @param node
     * @param output
     */
    abstract public void enter(Element node, OutputStream output) throws IOException;

    abstract public void leave(Element node, OutputStream output) throws IOException;
}
