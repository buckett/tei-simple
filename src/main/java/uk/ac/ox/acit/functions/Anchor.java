package uk.ac.ox.acit.functions;

import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by buckett on 18/03/15.
 */
public class Anchor extends XPathFunction {

    public Anchor(XPathExpression xpath) {
        super(xpath);
    }

    @Override
    public String getName() {
        return "anchor";
    }

    @Override
    public void enter(Element node, OutputStream output) throws IOException {

    }

    @Override
    public void leave(Element node, OutputStream output) throws IOException {

    }

}
