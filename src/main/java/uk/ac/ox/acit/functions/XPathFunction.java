package uk.ac.ox.acit.functions;

import org.jdom2.xpath.XPathExpression;

/**
 * Created by buckett on 18/03/15.
 */
public abstract class XPathFunction extends Function {

    private XPathExpression xpath;

    public XPathFunction(XPathExpression xpath) {
        this.xpath = xpath;
    }
}
