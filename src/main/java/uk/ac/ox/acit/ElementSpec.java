package uk.ac.ox.acit;

import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.located.LocatedElement;
import org.jdom2.util.IteratorIterable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * An element specification is what models are attached to.
 */
public class ElementSpec {

    @XmlAttribute
    private String ident;

    @XmlElement(name="model")
    private List<Model> models;

    @XmlElement(name="modelSequence")
    private List<ModelSequence> modelSequences;

    @XmlElement(name="modelGrp")
    private List<ModelGroup> modelGroups;

    public List<Model> getModels() {
        return models;
    }

    public String getIdent() {
        return ident;
    }

    public void transform(Element element, OutputStream out) throws IOException {
        if (models != null) {
            for (Model model: models) {
                if (model.matches(element)) {
                    model.transform(element, out);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "ElementSpec{" +
                "ident='" + ident + '\'' +
                ", models=" + models +
                ", modelSequences=" + modelSequences +
                ", modelGroups=" + modelGroups +
                '}';
    }
}
