package uk.ac.ox.acit;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * A model sequence allows multiple models to match on an element.
 */
public class ModelSequence {

    @XmlAttribute
    protected Boolean useSourceRendition;
    @XmlAttribute
    protected Output output;

    @Override
    public String toString() {
        return "ModelSequence{" +
                "useSourceRendition=" + useSourceRendition +
                ", output=" + output +
                '}';
    }
}
