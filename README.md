= TEI ODD Extensions Processing

This is a re-implementation of the TEI ODD extensions for processing models. This is primarily done to validate the
specification.

The sample processing file is teisimple.odd, it is both the documentation and an example of a set of ODD extensions.
This would have been easier to find if it was in a single file.

= Implementation

Process teisimple.odd to produce spec of how to transform document, then read in document and run it over the parsed spec.

Need to implement all the processing instructions in Java.

jdom2 choosen as it's reasonably up to date and supports xpath (only 1).

== Notes ==

Do I need to care about <rendition> tag, what about

<rendition scope="before"> - This can be supported in CSS, but needs more work for text.
<rendition scheme="css">

Logging of problems?

How to parse the xpath as it may contain commas.

Assuming we're using XPath 1.

Do we have function spec in code, or just function implementation? Depends if we want functions to be
extenable and plugable.

Why is . recursive when used as a function parameter? Wouldn't .// be a better fit? Because you only enclose one node.

What's the default output format when not specified on an element and what should the processor assume as the default output?

What happens when the xpath expression to a function doesn't return anything? Should the container element be output?

Could we just build a huge xpath and still use StAX? - Not easily because of the XPath expressions to the functions


== Load Spec

Simple XML processor, need to


= Links

Specification - https://github.com/TEIC/TEI-Simple/blob/master/tei-pm.html

Example tests - https://github.com/TEIC/TEI-Simple/tree/master/tests

http://blog.bdoughan.com/2012/08/handle-middle-of-xml-document-with-jaxb.html